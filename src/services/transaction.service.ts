const transactionPoolObj = require('pg').Pool
const transactionPool = new transactionPoolObj({
    user: 'me',
    host: 'localhost',
    database: 'budgeter',
    password: 'password',
    port: 5432,
})
const get = (request, response) => {
    transactionPool.query('SELECT * FROM users ORDER BY id ASC', (error,results) => {
        if(error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}
const getAll = (request, response) => {
    const id = parseInt(request.params.id)

    transactionPool.query('SELECT * FROM users WHERE id = $1', [id], (error,results) => {
        if(error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}
const create = (request,response) => {
    const { name, email } = request.body

    transactionPool.query('INSERT INTO users (name, email) VALUES ($1, $2)', [name, email], (error, results) => {
        if (error) {
            throw error
        }
        response.status(201).send(`User added with ID: ${results.insertId}`)
    })
}
const update = (request, response) => {
    const id = parseInt(request.params.id)
    const { name, email } = request.body
  
    transactionPool.query(
      'UPDATE users SET name = $1, email = $2 WHERE id = $3',
      [name, email, id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).send(`User modified with ID: ${id}`)
      }
    )
  }
  const remove = (request, response) => {
    const id = parseInt(request.params.id)
  
    transactionPool.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User deleted with ID: ${id}`)
    })
  }

  export = {
    get,
    getAll,
    create,
    update,
    remove
};