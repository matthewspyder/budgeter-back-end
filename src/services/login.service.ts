import url from 'url'
import { getSecretKey } from '../config/config';
import { dbconfig } from'../config/db.config';
import { encodeSession } from './sessions.service';

const signup = async (request,response) => {
    const buffers = [];
  
    for await (const chunk of request) {
      buffers.push(chunk);
    }
  
    const data = Buffer.concat(buffers).toString();
    const requestBody = JSON.parse(data);
    const { id, email, username, password, permissions, lighttheme } = requestBody;
    const createddate = new Date();
    console.log(createddate);
  
      dbconfig.query('INSERT INTO users (email, username, password, permissions, lighttheme, createddate) VALUES ($1, $2, $3, $4, $5, $6)', [email, username, password, permissions, lighttheme, createddate], (error, results) => {
          if (error) {
              throw error
          }
          response.status(201).send(`User added with email: ${email}`)
      })
  }
const login = async (request,response) => {
const buffers = [];

for await (const chunk of request) {
    buffers.push(chunk);
}

const data = Buffer.concat(buffers).toString();
const requestBody = JSON.parse(data);
const { username, password } = requestBody;
const createddate = new Date();

    dbconfig.query('SELECT * FROM users WHERE username=$1', [username], (error, results) => {
        if (error) {
            console.log(error);
            throw error
        }
        if(results.rows.length>0) {
            if(results.rows[0].password===password){
                response.status(200).json({
                    ok: true,
                    status: 200,
                    session: encodeSession(getSecretKey(), {
                        userid: results.rows[0].userid,
                        username: results.rows[0].username,
                        dateCreated: Date.now()        
                    }),
                    message: `success`
                })
            } else {
                response.status(401).json({
                    ok: false,
                    status: 401,
                    message: 'Please enter a valid password'
                });
            }
        } else {
            response.status(401).json({
                ok: false,
                status: 401,
                message: 'Please enter a valid username'
            });
        }
    })
}

export default {
    login,
    signup
}