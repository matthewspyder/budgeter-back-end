import url from 'url'
import { dbconfig } from'../config/db.config';

const getAll = (request, response) => {
    dbconfig.query('SELECT * FROM users ORDER BY userid ASC', (error,results) => {
        if(error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}
const get = (request, response) => {
    var parts = url.parse(request.url, true);
    const id = parts.path.slice(1);
    dbconfig.query('SELECT * FROM users WHERE userid = $1', [id], (error,results) => {
        if(error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}
const create = async (request,response) => {
  const buffers = [];

  for await (const chunk of request) {
    buffers.push(chunk);
  }

  const data = Buffer.concat(buffers).toString();
  const requestBody = JSON.parse(data);
  const { id, email, username, password, permissions, lighttheme } = requestBody;
  const createddate = new Date();

    dbconfig.query('INSERT INTO users (email, username, password, permissions, lighttheme, createddate) VALUES ($1, $2, $3, $4, $5, $6)', [email, username, password, permissions, lighttheme, createddate], (error, results) => {
        if (error) {
            throw error
        }
        response.status(201).send(`User added with email: ${email}`)
    })
}
const update = async (request, response) => {
    const buffers = [];

    for await (const chunk of request) {
      buffers.push(chunk);
    }
  
    const data = Buffer.concat(buffers).toString();
    const requestBody = JSON.parse(data);
    const { userid, email, username, password, permissions, lighttheme } = requestBody;
  
    dbconfig.query(
      'UPDATE users SET email=COALESCE($1, email), username=COALESCE($2, username), password=COALESCE($3, password), permissions=COALESCE($4, permissions), lighttheme=COALESCE($5, lighttheme) WHERE userid = $6',
      [email, username, password, permissions, lighttheme, userid],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).send(`modified with Account info: ${email}, ${username}, ${permissions},${lighttheme} ${userid}`)
      }
    )
  }
  const remove = (request, response) => {
    var parts = url.parse(request.url, true);
    const id = parts.path.slice(1);
    dbconfig.query('DELETE FROM users WHERE userid = $1', [id], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User deleted with ID: ${id}`)
    })
  }

export = {
    get,
    getAll,
    create,
    update,
    remove
};