const accountPoolObj = require('pg').Pool
const accountPool = new accountPoolObj({
    user: 'me',
    host: 'localhost',
    database: 'budgeter',
    password: 'password',
    port: 5432,
})

const getAll = (request, response) => {
    accountPool.query('SELECT * FROM accounts ORDER BY id ASC', (error,results) => {
        if(error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}
const get = (request, response) => {
    const id = parseInt(request.params.id)

    accountPool.query('SELECT * FROM accounts WHERE id = $1', [id], (error,results) => {
        if(error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}
const create = (request,response) => {
    const { accountName, accountProvider } = request.body

    accountPool.query('INSERT INTO accounts (accountName, accountProvider) VALUES ($1, $2)', [accountName,accountProvider], (error, results) => {
        if (error) {
            throw error
        }
        response.status(201).send(`Account added with ID: ${results.insertId}, accountName`)
    })
}
const update = (request, response) => {
    const id = parseInt(request.params.id)
    const { accountName, accountProvider } = request.body
  
    accountPool.query(
      'UPDATE accounts SET accountName = $1, accountProvider = $2 WHERE id = $3',
      [accountName, accountProvider, id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).send(`modified with Account info: ${accountName}, ${accountProvider} ${id}`)
      }
    )
  }
  const remove = (request, response) => {
    const id = parseInt(request.params.id)
  
    accountPool.query('DELETE FROM accounts WHERE id = $1', [id], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User deleted with ID: ${id}`)
    })
  }
export = {
    get,
    getAll,
    create,
    update,
    remove
};