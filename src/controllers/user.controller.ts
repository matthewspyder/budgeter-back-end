import userService from '../services/adminUser.service';

const get = function(req, res){
    res.send(userService.get(req, res));
}

const getAll = function(req, res){
    res.send(userService.getAll(req, res));
}

const create = function(req, res){
    res.send(userService.create(req, res));
}

const update = function(req, res){
    res.send(userService.update(req, res));
}

const remove = function(req, res){
    userService.remove(req, res);
}

export = {
    get,
    getAll,
    create,
    update,
    remove
}