import transactionService from '../services/transaction.service';

const get = function(req, res){
    res.send(transactionService.get(req, res));
}

const getAll = function(req, res){
    res.send(transactionService.getAll(req, res));
}

const create = function(req, res){
    res.send(transactionService.get(req, res));
}

const update = function(req, res){
    res.send(transactionService.get(req, res));
}

const remove = function(req, res){
    res.send(transactionService.get(req, res));
}

export = {
    get,
    getAll,
    create,
    update,
    remove
}