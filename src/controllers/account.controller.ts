import accountsService from '../services/account.service';

const get = function(req, res){
    res.send(accountsService.get(req, res));
}

const getAll = function(req, res){
    res.send(accountsService.getAll(req, res));
}

const create = function(req, res){
    res.send(accountsService.get(req, res));
}

const update = function(req, res){
    res.send(accountsService.get(req, res));
}

const remove = function(req, res){
    res.send(accountsService.get(req, res));
}

export = {
    get,
    getAll,
    create,
    update,
    remove
}