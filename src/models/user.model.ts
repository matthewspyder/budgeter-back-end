export interface IUser {
  userid: number;
  email: string;
  permissions: string;
  lighttheme: boolean;
}
export class User implements IUser
{
   constructor(public userid: number,
    public email: string,
    public password: string,
    public permissions: string,
    public lighttheme: boolean)
	{
	}
}
