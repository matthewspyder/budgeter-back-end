import express from "express";
import transactionService from '../services/transaction.service';

const router = express.Router();

router.route('/')
    .get(transactionService.getAll);

router.route('/:_id')
    .get(transactionService.get);

router.route('/')
    .post(transactionService.create);

router.route('/:id')
    .put(transactionService.update);

router.route('/:_id')
    .delete(transactionService.remove);

export = router;