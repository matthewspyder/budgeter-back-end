import express from "express";
import adminUserService from '../services/adminUser.service';
const router = express.Router();

router.route('/all')
    .get(adminUserService.getAll);

router.get('/:_id',adminUserService.get);

router.post('/',adminUserService.create);

router.put('/',adminUserService.update);

router.delete('/:_id', adminUserService.remove);

export = router;