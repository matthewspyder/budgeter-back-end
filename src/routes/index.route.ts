import express, { request } from "express";
import { Request, Response } from "express";
import admin from './admin.route';
import account from "./account.route";
import transaction from "./transaction.route";
import login from './login.route';
import auth from './auth.route'
import { requireJwtMiddleware } from "../middleware/authJwt";

  const router = express.Router();
  router.use('/admin',requireJwtMiddleware);
  router.use('/account', account);
  router.use('/auth',auth);
  router.use('/transaction', transaction);
  router.use('/admin', admin);
  router.use('/login', login);

  router.get('/', (req, res) => res.send('The Budgeter API'));
  router.get('/health', (req, res) => {
    const healthcheck = {
      uptime: process.uptime(),
      message: 'OK',
      timestamp: Date.now()
    };
    res.send(JSON.stringify(healthcheck));
  });
  export default router;