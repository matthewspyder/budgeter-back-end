import express from "express";
import { requireJwtMiddleware } from "../middleware/authJwt";

const router = express.Router();

router.get('/', requireJwtMiddleware);

export = router;