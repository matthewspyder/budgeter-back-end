import express from "express";
import user from "./adminUser.route";

const router = express.Router();

router.use('/user', user);

export = router;