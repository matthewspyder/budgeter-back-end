import express from "express";
import accountService from '../services/account.service';

const router = express.Router();

router.route('/')
    .get(accountService.getAll);

router.route('/:_id')
    .get(accountService.get);

router.route('/')
    .post(accountService.create);

router.route('/:id')
    .put(accountService.update);

router.route('/:_id')
    .delete(accountService.remove);

export = router;