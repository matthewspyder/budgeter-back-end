// import express from 'express';
import express from 'express';
import http from 'http';
import https from 'https';
// const bodyParser = require('body-parser')
/*const userQueries = require('./queries/userQueries.ts')
const accountQueries = require('./queries/accountQueries.ts')
const transactionQueries = require('./queries/transactionQueries.ts')*/
import cors from 'cors';
import routes from "./routes/index.route";
import * as Sentry from "@sentry/node";
import * as Tracing from "@sentry/tracing";
// const fs = require('fs')


//build config from params
import {buildConfigFromParams, setInitialSecret} from './config/config';
const {https:{ key, cert}, port, isHttps, serviceName} = buildConfigFromParams();
const credentials = {key, cert};

setInitialSecret();

//setup app & its routes
const app = express();

Sentry.init({
  dsn: "https://09f18dc0498e46e0b7d3de5f027a7bec@o1134971.ingest.sentry.io/6183451",
  integrations: [
    // enable HTTP calls tracing
    new Sentry.Integrations.Http({ tracing: true }),
    // enable Express.js middleware tracing
    new Tracing.Integrations.Express({ app }),
  ],

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});
// RequestHandler creates a separate execution context using domains, so that every
// transaction/span/breadcrumb is attached to its own Hub instance
app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());

app.use(cors({ origin: '*' , credentials :  true}));

app.get("/debug-sentry", function mainHandler(req, res) {
  throw new Error("My first Sentry error!");
});

app.use(routes);

// The error handler must be before any other error middleware and after all controllers
app.use(Sentry.Handlers.errorHandler());

// Optional fallthrough error handler
app.use(function onError(err, req, res, next) {
  // The error id is attached to `res.sentry` to be returned
  // and optionally displayed to the user for support.
  res.statusCode = 500;
  res.end(res.sentry + "\n");
});


//start http server
const httpServer = http.createServer(app);
httpServer.listen(port);
console.log(`[${serviceName}] http server listening at port ${port}`);

//start https server
if(isHttps) {
  const httpsServer = https.createServer(credentials, app);
  httpsServer.listen(port+1);
  console.log(`[${serviceName}] https server listening at port ${port + 1}`);
}

export = { };