const PoolObj = require('pg').Pool
const dbconfig = new PoolObj({
    user: 'me',
    host: 'localhost',
    database: 'budgeter',
    password: 'password',
    port: 5432,
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
})

export {
    dbconfig
}